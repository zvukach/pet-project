<?php

declare(strict_types=1);

$finder = PhpCsFixer\Finder::create()
    ->ignoreDotFiles(false)
    ->ignoreVCS(true)
    ->exclude([
        'vendor',
        'tests/_support',
    ])
    ->in([
        __DIR__ . '/src',
        __DIR__ . '/tests',
    ])
;

$rules = [
    '@Symfony' => true,
    '@PhpCsFixer' => true,
    'single_import_per_statement' => false,
    'concat_space' => ['spacing' => 'one'],
    'array_syntax' => ['syntax' => 'short'],
    'phpdoc_no_package' => false,
    'phpdoc_to_comment' => false,
    'no_superfluous_phpdoc_tags' => false,
    'ternary_to_null_coalescing' => true,
    'visibility_required' => true,
    'nullable_type_declaration_for_default_null_value' => true,
    'global_namespace_import' => [
        'import_classes' => true,
        'import_constants' => null,
        'import_functions' => null,
    ],
    'declare_strict_types' => true,
];

if (class_exists('\\PhpCsFixer\\Fixer\\ControlStructure\\YodaStyleFixer')) {
    $rules['yoda_style'] = false;
}
$config = new PhpCsFixer\Config();

return $config
    ->setRules($rules)
    ->setFinder($finder)
    ->setRiskyAllowed(true)
    ;
